# The (GCHQ) Puzzle Book 2
Government Communications Headquarters Puzzles


**Puzzles**

**Q1. The early bird**

What connects Skipper, Mumble, Wheezy, Pinga, Tux and Feathers McGraw?

**A1. They're all Penguins**

**Why?**

Skipper from Madagascar. Mumble from Happy Feet. Wheezy from Toy Story 2. Pinga from Pingu. Tux from the mascot of Linux. Feathers McGraw from Wallace and Gromit.

---
